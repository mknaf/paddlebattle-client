# About
A game client for PaddleBattle. See [the PaddleBattle server](https://github.com/mknaf/paddlebattle-server)
for more info.

# Setup
The `dist` folder is the root of the page. It must be created by using
the Makefile. This folder is then served by a dedicated web server.
## nginx
There is an example nginx config file in the repository folder. It sets up
a pretty default vhost. Additionally, a proxy route to the paddlebattle-server
is set up.

# Built with
 * [socket.io](http://socket.io)
 * [jQuery](http://jquery.com)
 * [semantic-ui](http://semantic-ui.com)
