all:

debug:
	rm -rf dist
	ln -s src dist

release:
	# copy complete src dir over
	rm -rf dist
	mkdir -p dist
	cp -rf src/* dist/
	#
	# if yui-compressor is not installed, this will fail and there
	# simply are no minified versions.
	#
	yui-compressor src/js/paddlebattle.js -o dist/js/paddlebattle.js

