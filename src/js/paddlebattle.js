"use strict";
var socket = io.connect("http://" + document.domain + ":" + location.port);

var paddles = [];

var game = new Phaser.Game(
    "100",  // width
    "100",  // height
    Phaser.AUTO,  // renderer
    null,  // parent
    {
        preload: preload,
        create: create,
        render: render,
        update: update
    }
);

/**
 * socket.io - basic handshake event handlers
 */
socket.on("connect", function() {
    console.log("Connected to server.");

    $("#disconn_warn").hide();

    socket.emit("join");
    console.log("Trying to join game.");
});
socket.on("disconnect", function() {
    $("#disconn_warn").show();

    game.destroy();

    console.log("Disconnected.");
});



/**
 * actual gameplay event handlers
 */

var get_player = function() {
    return {
        "mov": "stop",
        "img": null,
        "pos": 0.5
    };
};
var player_join = function() {
    /* Callback function for player_join event. */
    console.log("Player joined.");

    paddles.push(get_player());
};
var player_leave = function(player) {
    /* Callback function for player_leave event. */
    // get rid of that player
    paddles[player]["img"].destroy();
    paddles.splice(player, 1);

    console.log("Player " + player + " left.");
};
var init_game = function(state) {
    /* Callback function for init_game event. */
    console.log("Received initial_gamestate: ", state);

    for (var i = 0; i < state.length; i = i + 1) {
        paddles[i] = get_player();
    }

};
var paddle_move_left = function(player) {
    paddles[player]["mov"] = "left"
};
var paddle_move_right = function(player) {
    paddles[player]["mov"] = "right"
};
var paddle_move_stop = function(player) {
    paddles[player]["mov"] = "stop"
};



/**
 * socket.io - communication handlers
 */
socket.on("move_paddle", function(player, direction) {
    switch(direction) {
        case "stop":  // stop
            paddle_move_stop(player);
            break;
        case "right":  // right
            paddle_move_right(player);
            break;
        case "left":  // left
            paddle_move_left(player);
            break;
    };
});
socket.on("player_join", function(player) {
    player_join(player);
});
socket.on("player_leave", function(player) {
    player_leave(player);
});
socket.on("init_game", function(state) {
    init_game(state);
});
socket.on("get_gamestate", function(state) {
    for (var i = 0; i < paddles.length; i++) {
        paddles[i]["pos"] = state[i]["pos"];
    }
});




/**
 * keyboard event handlers
 */
var pressed_keys = {
    37: false,  // left
    39: false  // right
};
$(document).keydown(function(event) {
    // don't do anything if the key is already pressed.
    // without this, the event is fired repeatedly.
    if (pressed_keys[event.keyCode]) {
        return;
    }

    // decide what to send
    switch (event.keyCode) {
        case 37:  // left
            socket.emit(
                "move_paddle",
                "left"
            );
            break;
        case 39:  // right
            socket.emit(
                "move_paddle",
                "right"
            );
            break;
    };
    pressed_keys[event.keyCode] = true;
});
$(document).keyup(function(event) {
    switch (event.keyCode) {
        case 37:  // left
        case 39:  // right
            pressed_keys[event.keyCode] = false;

            // at this point at max 1 key is pressed
            if (pressed_keys[37]) {  // if left is still pressed
                socket.emit(
                    "move_paddle",
                    "left"
                );
            } else if (pressed_keys[39]) {  // if right is still pressed
                socket.emit(
                    "move_paddle",
                    "right"
                );
            } else {  // if no key is pressed anymore
                socket.emit(
                    "move_paddle",
                    "stop"
                );
            }
            break;
    };
});

/**
 * Phaser.io stuff
 */

var line_len = 0;
// how often to sync positions with the server
var server_sync_rate = 100;

function create() {

    // start requesting the gamestates on a regular basis
    window.setInterval(
        function() {
            socket.emit("get_gamestate");
        }, server_sync_rate
     );

    var display_max = (game.world.width < game.world.height) ? game.world.width : game.height

    line_len = game.world.width;

    // draw a visible circle
    var graphics = game.add.graphics(
        game.world.centerX,
        game.world.centerY
    );
    graphics.lineStyle(
        4,  // width
        0xaf3fdf,  // color
        0.5  // opacity
    );
    graphics.drawCircle(0, 0, display_max);

    // create an invisible circle object to work on
    var circle = new Phaser.Circle(0, 0, display_max);

    // get 3 points on the circle
    var a = circle.circumferencePoint(0);
    var b = circle.circumferencePoint(120, true);
    var c = circle.circumferencePoint(240, true);

    // draw lines bewteen all points
    graphics.moveTo(a.x, a.y);
    graphics.lineTo(b.x, b.y);
    graphics.lineTo(c.x, c.y);
    graphics.lineTo(a.x, a.y);

};
function preload() {
    game.load.image("sprite", "assets/pics/sprite-can-small.png");
    game.stage.disableVisibilityChange = true;
};
function render() {
};
var last_server_sync = $.now();
function update() {

    // for all the players
    for (var i = 0; i < paddles.length; i = i + 1) {

        // if this player doesn't have a paddle yet
        if (paddles[i]["img"] == null) {
            var img = game.add.sprite(
                paddles[i]["pos"] * line_len,
                i * 100,
                "sprite"
            );
            game.physics.enable(img, Phaser.Physics.ARCADE);

            paddles[i]["img"] = img;
        }
    }

    // for all the players in the game
    for (var i = 0; i < paddles.length; i++) {

        if (paddles[i]["img"] == null) {
            continue;
        }

        // apply gamestate changes to graphics
        if (paddles[i].mov == "left") {
            paddles[i]["img"].body.velocity.x = -250;
        } else if (paddles[i].mov == "right") {
            paddles[i]["img"].body.velocity.x = 250;
        } else {
            paddles[i]["img"].body.velocity.x = 0;
        }

        // sync with the server once in a while
        if ($.now() - last_server_sync >= server_sync_rate) {
            paddles[i]["img"].x = paddles[i]["pos"] * line_len;
        }
    }
};
